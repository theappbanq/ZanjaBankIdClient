/**
 * Created by mohammad on 3/13/16.
 */


var zanjaClient = require('../ZanjaBankIdClient');

module.exports = {
    setUp: function (callback) {
        zanjaClient.setServerUri('http://37.48.109.121:8799/bankid');
        callback();
    },
    tearDown: function (callback) {
        // clean up
        callback();
    },

    auth: function (test) {
        zanjaClient.auth("197807086330", function(result, err) {
            test.equal(err, null);
            test.notEqual(result, null);
            console.log(JSON.stringify(result));
            test.done();
        }, 180);
    },

    collect: function (test) {
        zanjaClient.collect("a4e3ffd8-6092-408f-b9b8-a2f87560f75a", function(result, err) {
            test.equal(err, null);
            test.notEqual(result, null);
            console.log(JSON.stringify(result));
            test.done();
        });
    },

    sign: function (test) {
        zanjaClient.sign("197807086330", "This is the visible text.", "This is the none visible text.", function(result, err) {
            test.equal(err, null);
            test.notEqual(result, null);
            console.log(JSON.stringify(result));
            test.done();
        }, 180);
    }
}