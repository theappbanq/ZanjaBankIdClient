/**
 * Created by mohammad on 3/13/16.
 */

var req     = require('request');

module.exports = {

    collectInterval: 5000,
    setCollectInterval: function (interval) {
        this.collectInterval = interval;
    },

    serverUri: null,
    setServerUri: function (uri) {
        this.serverUri = uri;
    },

    errors: {
        Timeout: "Timeout",
        InternalError: "InternalError"
    },

    auth: function (personalId, cb, timeout) {

        if(!this.serverUri) {
            throw new Error("Please set server uri first.");
        }

        var self = this;
        var options = {
            url: self.serverUri + '/Auth/' + personalId,
            headers: {
                'Accept': 'application/json'
            }
        };

        function callback(error, response, body) {
            if (!error && response.statusCode == 200) {
                var result = JSON.parse(body);
                console.log(body);
                self.getResult(result.orderId, cb, timeout);
            } else {
                cb(null, self.errors.InternalError);
            }
        }

        req.post(options, callback);
    },

    getResult: function (orderRef, cb, timeout) {
        var self = this;
        var collectInterval = this.collectInterval;
        var collectCallCounts = 0;

        function isFinalResult(collectResult) {
            if (collectResult.status !== undefined && collectResult.status == 5) {
                return true;
            }

            return false;
        }

        function collectCallBack(collectResult, err) {
            if(err) {
                cb(null, err);
            } else {
                if(isFinalResult(collectResult)) {
                    cb(collectResult);
                } else {

                    if(collectCallCounts * collectInterval > timeout * 1000) {
                        cb(null, self.errors.Timeout);
                    } else {
                        setTimeout(function () {
                            collectCallCounts++;
                            self.collect(collectResult.orderId, collectCallBack);
                        }, collectInterval);
                    }
                }
            }
        }

        setTimeout(function() {
            collectCallCounts++;
            self.collect(orderRef, collectCallBack);
        }, collectInterval);
    },

    collect: function (orderRef, cb) {
        if(!orderRef) {
            cb(null, "OrderRef can't be null");
        }

        var self = this;
        var options = {
            url: self.serverUri + '/Collect/' + orderRef,
            headers: {
                'Accept': 'application/json'
            }
        };

        function callback(error, response, body) {
            if (!error && response.statusCode == 200) {
                var result = JSON.parse(body);
                console.log(body);
                cb(result);
            } else {
                cb(null, self.errors.InternalError);
            }
        }

        req.get(options, callback);
    },

    sign: function (personalNumber, visibleText, nonVisibleText, cb, timeout) {

        if(visibleText === undefined || nonVisibleText === undefined) {
            cb(null, "Null or undefined argument");
            return;
        }

        var self = this;
        var options = {
            url: self.serverUri + '/Sign',
            headers: {
                'Accept': 'application/json'
            },
            formData: {
                personalNumber: personalNumber,
                visibleText: visibleText,
                nonVisibleText: nonVisibleText
            }
        };

        function callback(error, response, body) {
            if (!error && response.statusCode == 200) {
                var result = JSON.parse(body);
                console.log(body);
                self.getResult(result.orderId, cb, timeout);
            } else {
                cb(null, self.errors.InternalError);
            }
        }

        req.post(options, callback);
    }
};

